package application;

import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Optional;
import java.util.ResourceBundle;

import javax.swing.GroupLayout.Alignment;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.controlsfx.control.textfield.TextFields;

import com.jfoenix.controls.JFXTextField;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;

public class EleveController implements Initializable {
	@FXML
	private StackPane contentPane;
	@FXML
	private TableView<Eleve> eleveTable;

	@FXML
	private TableColumn<Eleve, Integer> numCol;

	@FXML
	private TableColumn<Eleve, String> nomCol;

	@FXML
   private TableColumn<Eleve, String> prenomCol;

	@FXML
	private TableColumn<Eleve, String> moyCol;
	@FXML
	private JFXTextField txtNum;

	@FXML
	private JFXTextField txtNom;

	@FXML
	private JFXTextField txtPrenom;
	@FXML
	private JFXTextField txtEmail;
	@FXML
	private TextField txtChercher;

	@FXML
	ObservableList<Eleve> eleves = FXCollections.observableArrayList();

	@FXML
	void imprimer(ActionEvent event) throws IOException {
		String chemin="";
		DirectoryChooser directoryChooser = new DirectoryChooser();
		File dir = directoryChooser.showDialog(new Stage());
		if (dir != null) {
			chemin+= dir.getAbsolutePath();
			}
		String cheminComplet=chemin+"\\"+"ListeDesEleves.xls";

		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet("Eleves");
		ObservableList<Eleve> listEleves = eleves;

		int rownum = 1;
		Cell cell;
		Row row;
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 2));
		//
		HSSFCellStyle style = createStyleForTitle(workbook);
		// titre
		row = sheet.createRow(0);
		cell=row.createCell(0);
		CellStyle cellstyle1=workbook.createCellStyle();
		cellstyle1.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		HSSFFont font = workbook.createFont();
		font.setBold(true);
		cellstyle1.setFont(font);
		cell.setCellValue(" UPGRADE Group");
		cell.setCellStyle(cellstyle1);

		row = sheet.createRow(rownum);
		CellStyle cellstyle=workbook.createCellStyle();
		cellstyle.setAlignment(HorizontalAlignment.CENTER_SELECTION);
		

		// Numero eleve
		cell = row.createCell(0, CellType.NUMERIC);
		cell.setCellValue("Numero");
		cell.setCellStyle(style);
		// NOm Eleve
		cell = row.createCell(1, CellType.STRING);
		cell.setCellValue("Nom");
		cell.setCellStyle(style);
		// Prenom eleve
		cell = row.createCell(2, CellType.STRING);
		cell.setCellValue("Prenom");
		cell.setCellStyle(style);
		// Moyenne
		cell = row.createCell(3, CellType.NUMERIC);
		cell.setCellValue("Email");
		cell.setCellStyle(style);

		// Data
		for (Eleve elv : listEleves) {
			rownum++;
			row = sheet.createRow(rownum);

			cell = row.createCell(0, CellType.NUMERIC);
			cell.setCellValue(elv.getId());
			cell.setCellStyle(cellstyle);
			// elvName (B)
			cell = row.createCell(1, CellType.STRING);
			cell.setCellValue(elv.getNom());
			// eleve prenom (C)
			cell = row.createCell(2, CellType.STRING);
			cell.setCellValue(elv.getPrenom());
			// moyenne eleve (D)
			cell = row.createCell(3, CellType.STRING);
			cell.setCellValue(elv.getEmail());
			 
		}
		File file = new File(cheminComplet);
		file.getParentFile().mkdirs();

		FileOutputStream outFile = new FileOutputStream(file);
		workbook.write(outFile);
		System.out.println("Created file: " + file.getAbsolutePath());

		alerter(file);

	}

	public void alerter(File f) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Ouverture");
		alert.setHeaderText("VoulezVous ouvrir le fichier maintenant");
		// alert.setContentText("C:/MyFile.txt");

		// option != null.
		Optional<ButtonType> option = alert.showAndWait();

		if (option.get() == ButtonType.OK) {
			try {
				Desktop.getDesktop().open(f);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (option.get() == ButtonType.CANCEL) {

		}

	}

	 

	public void initialize(URL location, ResourceBundle resources) {
		numCol.setCellValueFactory(new PropertyValueFactory<Eleve, Integer>("id"));
		nomCol.setCellValueFactory(new PropertyValueFactory<Eleve, String>("nom"));
		prenomCol.setCellValueFactory(new PropertyValueFactory<Eleve, String>("prenom"));
		moyCol.setCellValueFactory(new PropertyValueFactory<Eleve, String>("email"));

		// eleves = EleveDAO.listEleves();
		// eleveTable.setItems(eleves);
		afficher();
	}

	public void afficher() {
		eleves.clear();
		try {
			Connection con = ConnexionBD.getConnexion();
			String sql = "select * from eleve ;";
			PreparedStatement stm = con.prepareStatement(sql);
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				eleves.add(new Eleve(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		FilteredList<Eleve> filteredFourn = new FilteredList<>(eleves, b -> true);

		txtChercher.textProperty().addListener((observable, oldValue, newValue) -> {

			filteredFourn.setPredicate(el -> {
				if (newValue == null || newValue.isEmpty()) {
					return true;
				}

				String LowerCaseFilter = newValue.toLowerCase();
				if (String.valueOf(el.getId()).indexOf(LowerCaseFilter) != -1) {
					return true;
				} else if (el.getNom().toLowerCase().indexOf(LowerCaseFilter) != -1) {
					return true;
				} else if (el.getEmail().indexOf(LowerCaseFilter) != -1) {
					return true;
				} else {
					return false;
				}
			});
		});

		SortedList<Eleve> sortedDate = new SortedList<>(filteredFourn);

		txtChercher = TextFields.createClearableTextField();
		eleveTable.setItems(sortedDate);

	}

	public static Eleve selectedEleve;

	@FXML
	void chargerDetails(MouseEvent event) {
		if (event.getClickCount() == 2) {
			selectedEleve = eleveTable.getSelectionModel().getSelectedItem();
			Stage primaryStage = new Stage();
			try {
				Parent root = FXMLLoader.load(getClass().getResource("Note.fxml"));
				Scene scene = new Scene(root);
				// scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.show();
				contentPane.getScene().getWindow().hide();
			} catch (Exception e) {
				e.printStackTrace();
			}

		} else if (event.getClickCount() == 1) {
			Eleve e = eleveTable.getSelectionModel().getSelectedItem();
			txtNum.setText(e.getId() + "");
			txtNom.setText(e.getNom());
			txtPrenom.setText(e.getPrenom());
			txtEmail.setText(e.getEmail());
		}

	}

	public HSSFCellStyle createStyleForTitle(HSSFWorkbook workbook) {
		HSSFFont font = workbook.createFont();
		font.setBold(true);
		HSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		return style;
	}

	@FXML
	void deconnexion(ActionEvent event) throws Exception {
		 
		Main main = new Main();
		Stage stage  = new Stage();
		main.start(stage);
		contentPane.getScene().getWindow().hide();

	}

	@FXML
	void ajouterEleve(ActionEvent event) {
		if (!txtNom.getText().isEmpty() || !txtPrenom.getText().isEmpty() || !txtNum.getText().isEmpty()) {

			try {

				Connection con = ConnexionBD.getConnexion();
				String sql = "INSERT INTO eleve (nom,prenom,email) VALUES (?,?,?)";
				PreparedStatement stm = con.prepareStatement(sql);
				stm.setString(1, txtNom.getText());
				stm.setString(2, txtPrenom.getText());
				stm.setString(3, txtEmail.getText());

				int i = stm.executeUpdate();

				if (i > 0) {
					clear();
					Alert alert = new Alert(AlertType.INFORMATION);
					alert.setTitle("Ajout");
					//alert.setHeaderText("Look, an Information Dialog");
					alert.setContentText("L'eleve  "+txtNom.getText()+ " a ete ajout�");

					alert.showAndWait();
					afficher();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}// con.close();

				int idEleve = recupererID();
				System.out.println(idEleve);
				String matiere = "";
				for (int j = 0; j < 10; j++) {
					switch (j) {
					case 0:
						matiere = "Infos";
						break;
					case 1:
						matiere = "Maths";
						break;
					case 2:
						matiere = "Dictee";
						break;
					case 3:
						matiere = "Grammaire";

						break;
					case 4:
						matiere = "Sciences";
						break;
					case 5:
						matiere = "Physique";
						break;
					case 6:
						matiere = "Arithmetique";
						break;
					case 7:
						matiere = "Orthographe";
						break;
					case 8:
						matiere = "EPS";
						break;
					case 9:
						matiere = "TM";
						break;
						
						
					default:
						break;
					}
					try {
						Connection con2 = ConnexionBD.getConnexion();
						String sql2 = "INSERT INTO note (matiere,note,appreciation, idEl) VALUES (?,?,?,?)";
						PreparedStatement stm2 = con2.prepareStatement(sql2);
						stm2.setString(1, matiere);
						stm2.setDouble(2, 0.0);
						stm2.setNString(3, "");
						stm2.setInt(4, idEleve);

						int k = stm2.executeUpdate();

						if (k > 0) {
							System.out.println("reussiiiiii linse");
						}

					} catch (Exception e) {
						e.printStackTrace();
					}
				}

		}
		clear();
	}

	public int recupererID() {
		int id = 0;
		try {
			Connection con = ConnexionBD.getConnexion();
			String sql = "select MAX(idEl ) from eleve ;";
			PreparedStatement stm = con.prepareStatement(sql);
			ResultSet rs = stm.executeQuery();

			if (rs.next()) {
				id = rs.getInt(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;

	}

	void clear() {
		txtEmail.clear();
		txtNom.clear();
		txtPrenom.clear();
		txtNum.clear();
	}

	@FXML
	void update(ActionEvent event) {
		try {

			Connection con = ConnexionBD.getConnexion();
			String sql = "UPDATE eleve set nom=?,prenom=?,email=? where idEl=? ";
			PreparedStatement stm = con.prepareStatement(sql);
			stm.setString(1, txtNom.getText());
			stm.setString(2, txtPrenom.getText());
			stm.setString(3, txtEmail.getText());
			stm.setInt(4, Integer.parseInt(txtNum.getText()));

			int i = stm.executeUpdate();

			if (i > 0) {
				clear();
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("mise a jour");
				alert.setHeaderText("");
			    alert.setContentText("les modifications ont �t�s prises en compte");
				alert.showAndWait();
			    afficher();
			}
			con.close();
			clear();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void Ddelete(ActionEvent event) {
		Eleve e = eleveTable.getSelectionModel().getSelectedItem();
		if (e != null) {
			Alert alert = new Alert(AlertType.CONFIRMATION);
			alert.setTitle("Suppression");
			alert.setHeaderText("Voulez vous vraiment suppprimer cet eleve ?");

			Optional<ButtonType> option = alert.showAndWait();

			if (option.get() == ButtonType.OK) {
				try {

					Connection con = ConnexionBD.getConnexion();
					String sql = "DELETE FROM Eleve WHERE idEl =?";
					PreparedStatement stm = con.prepareStatement(sql);
					stm.setInt(1, e.getId());
					int i = stm.executeUpdate();
					if (i > 0) {

						afficher();
						clear();
					}

				} catch (Exception ex) {
					ex.printStackTrace();
			}

			}
		}
	}
	@FXML
    void refresh(ActionEvent event) {
		afficher();
		clear();

    }

}
