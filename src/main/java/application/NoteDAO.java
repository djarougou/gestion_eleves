package application;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class NoteDAO {
	
	public static ObservableList<Note> listeNotes(){
		
		ObservableList<Note> notes = FXCollections.observableArrayList();
		Note n1 = new Note("Informatique", 18, " excellent ");
		Note n2 = new Note("Francais", 14, " bien ");
		Note n3 = new Note("Arithmetique", 10, " passable");
		Note n4 = new Note("Science", 0, " ");
		Note n5 = new Note("Dictee", 0, "  ");
		Note n6 = new Note("Histoire", 12, " A B ");
		Note n7 = new Note("Geographie", 14, " Bien ");
		Note n8 = new Note("ECM", 15, " Bien ");
		Note n9 = new Note("Problem", 10 ," Passable ");
		Note n10 = new Note("Grammaire", 17, "T Bien  ");
		Note n11 = new Note("EPS", 11, " passable ");
		Note n12 = new Note("TM", 16, "Tres bien");
		notes.addAll(n1,n2,n3,n4,n5, n6,n7,n8,n9, n10,n11, n12);
		
		return notes;
	}

}
